import numpy as np
from matplotlib.pyplot import step, show, axis

msg_original = "teste maroto"
manchester = ""
saida_em_binario = ""
msg_saida = ""
letra = ""
print("Mensagem original:\t\t", msg_original)
bin_tmp = ' '.join('{0:08b}'.format(ord(x), 'b') for x in msg_original)
msg_em_binario = bin_tmp.replace(" ","")

print("Mensagem original em binário:\t", msg_em_binario)

for x in range(len(msg_em_binario)):
	if msg_em_binario[x] == "0":
		manchester += "01"
	if msg_em_binario[x] == "1":
		manchester += "10"
	#print(msg_em_binario[x]," -> ", manchester)
print("Mensagem original codificada:\t",manchester)

for x in range(0, (len(manchester)), 2):
	if manchester[x] == "0" and manchester[x+1] == "1":
		saida_em_binario += "0"
	if manchester[x] == "1" and manchester[x+1] == "0":
		saida_em_binario += "1"
		
print("Mensagem recebida em binário:\t",saida_em_binario)

if(saida_em_binario == msg_em_binario):
	print("######\to binário recebido é igual ao binário enviado\t######\n")
else:
	print("######\tos binários não conferem\t######\n")

msg_saida += ''.join(chr(int(saida_em_binario[i*8:i*8+8],2)) for i in range(len(saida_em_binario)//8))

if(msg_saida == msg_original):
	print("######\ta mensagem recebida é igual a mensagem enviada\t######\n")
else:
	print("######\tas mensagens não conferem\t######\n")
	
print(msg_saida)

data = []
for x in range(len(msg_em_binario)):
	data.append(int(msg_em_binario[x]))

xaxis = np.arange(0, len(msg_em_binario))
yaxis = np.array(data)
step(xaxis, yaxis, color='r', linewidth=2.0)
#	xmin, xmax, ymin, ymax
axis([0, len(msg_em_binario)+1, 0, 1])
show()







