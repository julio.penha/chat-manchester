# Chat usando codificação Manchester

- Interface gráfica (deverá mostrar: msg escrita, msg em binário, msg em aplicado o algoritmo e o grafico) -> 1,0

- A forma de onda deve ser mostrada na forma de gráfico após a transformação com a sequência aplicada no algoritmo (forma de onda deverá ser mostrado em ambos os lados) -> 2,5

- Comunicação: entre dois ou mais computadores -> 2,0

- Mensagem dever ser transformada em binário utilizando a tabela ASCII estendido para dar correspondência (levar em consideração letras especiais e com acento) -> 0,5

- Depois de transformada, deve-se aplicar o princípio do algoritmo escolhido para transformar a mensagem -> 2,0

- A mensagem dever ser enviada para o outro lado pela rede -> 1,0

- O outro lado deve ser capaz de realizar o processo inverso e desmontar todo o bloco até reconhecer a msg -> 1,0